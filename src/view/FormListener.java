package view;

import java.util.EventListener;
/**
 *	Interface for transferring FormEvent data between classes.
 */
public interface FormListener extends EventListener {
	public void formEventOccurred(FormEvent fe) throws IllegalInput;
}
