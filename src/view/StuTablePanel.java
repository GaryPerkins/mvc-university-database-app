package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
/**
 *	Panel responsible for displaying Student data via a JTable.
 *	Is located within a JTabbedPane along with FacTablePanel.
 *  User can right click on a row to update or delete the object
 *	or can left click on a row to display the object it's associated
 *	with's information.
 */
public class StuTablePanel extends JPanel {
	private JTable table;
	private StudentTableModel studentModel;
	private JPopupMenu popup;
	private TableDeleteListener tableDeleteListener;
	private UpdateSetupListener updateSetupListener;
	private DisplaySelectedRowListener displaySelectedRowListener;
	
	public StuTablePanel() {
		studentModel = new StudentTableModel();
		table = new JTable(studentModel);
		popup = new JPopupMenu();
		JMenuItem removeItem = new JMenuItem("Delete Student");
		JMenuItem updateItem = new JMenuItem("Update Student");
		popup.add(updateItem);
		popup.add(removeItem);
		
		table.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				table.getSelectionModel().setSelectionInterval(row, row);
				if(e.getButton() == MouseEvent.BUTTON3) {
					popup.show(table, e.getX(), e.getY());
				}
				if(e.getButton() == MouseEvent.BUTTON1) {
					int rowSelected = table.getSelectedRow();
					String id = (String) table.getValueAt(rowSelected, 0);
					if(displaySelectedRowListener != null) {
						displaySelectedRowListener.idOfSelectedRow(id);
					}
				}
			}
		});
		
		updateItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int row = table.getSelectedRow();
				String id = (String) table.getValueAt(row, 0);
				UpdateSetup ue = new UpdateSetup(this, id);
				if(updateSetupListener != null) {
					updateSetupListener.updateSetupOccurred(ue);
					studentModel.fireTableDataChanged();
				}
			}
		});
		
		removeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int row = table.getSelectedRow();
				String id = (String) table.getValueAt(row, 0);
				if(tableDeleteListener != null) {
					tableDeleteListener.idOfDeletedRow(id);
					studentModel.fireTableDataChanged();
				}
			}
		});
		
		setLayout(new BorderLayout());
		add(new JScrollPane(table), BorderLayout.CENTER);
	}
	
	public void setTableDeleteListener(TableDeleteListener tableDeleteListener) {
		this.tableDeleteListener = tableDeleteListener;
	}
	
	public void setUpdateSetupListener(UpdateSetupListener updateSetupListener) {
		this.updateSetupListener = updateSetupListener;
	}
	
	public void setDisplaySelectedRowListener(DisplaySelectedRowListener displaySelectedRowListener) {
		this.displaySelectedRowListener = displaySelectedRowListener;
	}
	
	public void setStudentData(List list) {
		studentModel.setData(list);
	}
	
	public void refresh() {
		studentModel.fireTableDataChanged();
	}
}
