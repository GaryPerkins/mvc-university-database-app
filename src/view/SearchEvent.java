package view;

import java.util.EventObject;
/**
 *	Event generated from the SearchPanel class. This Event contains the ID
 *	to be searched for.
 */
public class SearchEvent extends EventObject {
	private String searchId;
	
	public SearchEvent(Object source, String searchId) {
		super(source);
		this.searchId = searchId;
	}
	
	public String getSearchId() {
		return searchId;
	}
}
