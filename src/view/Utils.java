package view;
/**
 *	Utility class for determining file extensions for the PersonFileFilter class.
 */
public class Utils {
	public static String getFileExtension(String name) {
		int pointIndex = name.lastIndexOf('.');
		if(pointIndex == -1) {
			return null;
		}
		if(pointIndex == name.length() - 1) {
			return null;
		}
		return name.substring(pointIndex + 1, name.length());
	}
}
