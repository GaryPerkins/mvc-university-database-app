package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
/**
 *	Component of the toolbar that allows the user to switch between different form types.
 *	Selecting the Student radio button will populate the FormPanel with labels and fields
 *	relevant to the creation of a Student object, whereas selection of the Faculty button
 *	will populate the FormPanel with labels and fields relevant for the creation of a Faculty
 *	object. By default, the Student button is selected at startup.
 */
public class FormTypeBar extends JPanel {
	private JLabel typeLabel = new JLabel("Select form type: ");
	private JRadioButton stuBtn;
	private JRadioButton facBtn;
	private ButtonGroup bg;
	private FormTypeListener formTypeListener;
	
	public FormTypeBar() {
		stuBtn = new JRadioButton("Student");
		stuBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				FormTypeEvent fte = new FormTypeEvent(this, stuBtn);
				formTypeListener.formTypeEventOccured(fte);
			}
		});
		facBtn = new JRadioButton("Faculty");
		facBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				FormTypeEvent fte = new FormTypeEvent(this, facBtn);
				formTypeListener.formTypeEventOccured(fte);
			}
		});
		
		bg = new ButtonGroup();
		bg.add(stuBtn);
		bg.add(facBtn);
		stuBtn.setSelected(true);
		
		add(typeLabel);
		add(stuBtn);
		add(facBtn);
	}
	
	public void setFormTypeListener(FormTypeListener formTypeListener) {
		this.formTypeListener = formTypeListener;
	}
	
	public JRadioButton getStuBtn() {
		return stuBtn;
	}
	public JRadioButton getFacBtn() {
		return facBtn;
	}
}
