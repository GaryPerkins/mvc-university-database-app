package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *	JPanel within the toolbar that allows a user to search via an ID number
 *	of an object. User can also set up the FormPanel for an update by 
 *	typing in the ID number of a to-be-updated object and clicking the update
 *	button; this will fill out the FormPanel fields with the information currently
 *	on file for the object to be updated.
 */
public class SearchPanel extends JPanel {
	private JLabel searchLabel;
	private JTextField searchField;
	private JButton searchBtn;
	private JButton updateBtn;
	private SearchListener searchListener;
	private UpdateSetupListener updateListener;
	
	public SearchPanel() {
		searchLabel = new JLabel("Search via ID: ");
		searchField = new JTextField(8);
		searchBtn = new JButton("Search");
		updateBtn = new JButton("Update");
		searchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String searchId = searchField.getText();
				SearchEvent se = new SearchEvent(this, searchId);
				if(searchListener != null) {
					searchListener.searchEventOccurred(se);
				}
			}
		});
		updateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String updateId = searchField.getText();
				UpdateSetup ue = new UpdateSetup(this, updateId);
				if(updateListener != null) {
					updateListener.updateSetupOccurred(ue);
				}
			}
		});
		add(searchLabel);
		add(searchField);
		add(searchBtn);
		add(updateBtn);
	}
	
	public void setSearchListener(SearchListener searchListener) {
		this.searchListener = searchListener;
	}
	
	public void setUpdateListener(UpdateSetupListener updateListener) {
		this.updateListener = updateListener;
	}
	
}
