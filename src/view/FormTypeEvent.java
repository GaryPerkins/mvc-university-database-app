package view;

import java.util.EventObject;

import javax.swing.JRadioButton;
/**
 *	Event object created when a user selects a RadioButton within the FormTypeBar class.
 *	Carries information on which button was selected to the MainFrame (or anywhere the
 *	developer desires).
 */
public class FormTypeEvent extends EventObject {
	private JRadioButton selected;
	public FormTypeEvent(Object source, JRadioButton selected) {
		super(source);
		this.selected = selected;
	}

	public JRadioButton getSelected() {
		return selected;
	}

	
}
