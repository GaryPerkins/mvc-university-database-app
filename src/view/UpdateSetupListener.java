package view;

import java.util.EventListener;
/**
 * Interface that allows the transmission of an UpdateSetup event object between components
 * in preparation for an object update.
 */
public interface UpdateSetupListener extends EventListener {
	public void updateSetupOccurred(UpdateSetup ue);
}
