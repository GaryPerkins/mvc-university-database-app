package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import model.Faculty;
import model.FullTimeFaculty;
import model.FullTimeStudent;
import model.PartTimeFaculty;
import model.PartTimeStudent;
import model.Person;
import model.Student;
import controller.Controller;
/**
 * @author Gary Perkins
 * 
 *	MainFrame class for presentation of GUI component of the database project. All interactions are done within this frame.
 *	The user has the ability to load data from a database and save back to it once finished. The user can also search for,
 *	update, and destroy a variety of database objects (Fulltime student/faculty or Parttime student/faculty). Data is represented
 *	in a JTable specific to the object type (student or faculty) within the frame. Either through clicking on a table row or using
 *	the searchbar provided via an object's ID number, the information about that object is displayed in a text area next to the table.
 *	Right-clicking on a row provides alternative routes to manipulate data if the user so desires. Once the user attempts to close
 *	a session, the program will detect if changes have been made within the current session and, if so, ask the user if they would like
 *	to save their changes to the database before the program exits. 
 */
public class MainFrame extends JFrame {
	
	private TextPanel textPanel;
	private FormTypeBar formTypeBar;
	private SearchPanel searchPanel;
	private FormPanel formPanel;
	private JTabbedPane tabbedPane;
	private StuTablePanel stuTablePanel;
	private FacTablePanel facTablePanel;
	private JPanel toolbar;
	private Controller controller;
	private JFileChooser fileChooser;
	private PrefsDialog prefsDialog;
	private Preferences prefs;
	private boolean existingChanges;
	
	public MainFrame() {
		super("Suffolk Community College Database");
		
		controller = new Controller();
		textPanel = new TextPanel();
		formTypeBar = new FormTypeBar();
		searchPanel = new SearchPanel();
		formPanel = new FormPanel();
		toolbar = new JPanel();
		toolbar.setLayout(new BorderLayout());
		toolbar.add(searchPanel, BorderLayout.CENTER);
		toolbar.add(formTypeBar, BorderLayout.SOUTH);
		toolbar.add(new JLabel(new ImageIcon("images/logo.png")), BorderLayout.NORTH);
		
		tabbedPane = new JTabbedPane();
		stuTablePanel = new StuTablePanel();
		stuTablePanel.setStudentData(controller.getStudents());
		facTablePanel = new FacTablePanel();
		facTablePanel.setFacultyData(controller.getFaculty());
		
		prefsDialog = new PrefsDialog(MainFrame.this);
		prefs = Preferences.userRoot().node("db");
		prefsDialog.setPrefsListener(new PrefsListener() {
			public void preferencesSet(String user, String password, int port) {
				prefs.put("user", user);
				prefs.put("password", password);
				prefs.putInt("port", port);
			}
		});
		
		//default values if none provided
		String user = prefs.get("user", "");
		String password = prefs.get("password", "");
		int port = prefs.getInt("port", 3306);
		
		prefsDialog.setDefaults(user, password, port);
		
		
		// file chooser things
		fileChooser = new JFileChooser();
		fileChooser.addChoosableFileFilter(new PersonFileFilter());
		setJMenuBar(createMenuBar());
		
		
		/*
		 * Listener that changes the FormPanel fields based on 
		 * the Form Type radio button selected.
		 */
		formTypeBar.setFormTypeListener(new FormTypeListener() {

			@Override
			public void formTypeEventOccured(FormTypeEvent e) {
				if(e.getSelected() == formTypeBar.getStuBtn()) {
					formPanel.clearFields();
					formPanel.createStudentBorder();
					formPanel.setStudentVisible();
				}
				if(e.getSelected() == formTypeBar.getFacBtn()) {
					formPanel.clearFields();
					formPanel.createFacultyBorder();
					formPanel.setFacultyVisible();
				}
			}
		});
		
		/* Listener that processes form data. If a new object with
		 * an existing id is being created, the listener will inform
		 * the user that the ID is currently in use.
		 */
		formPanel.setFormListener(new FormListener() {
			public void formEventOccurred(FormEvent e) throws IllegalInput {
				if(e.getCmd().equals("Student")) {
					List<Student> students = controller.getStudents();
					List<Faculty> faculty = controller.getFaculty();
					boolean found = false;
					
					for(int i = 0; i < students.size(); i++) {
						String id = students.get(i).getId();
						if(id.equals(e.getId())) {
							found = true;
						}
					}
					for(int i = 0; i < faculty.size(); i++) {
						String id = faculty.get(i).getId();
						if(id.equals(e.getId())) {
							found = true;
						}
					}
					
					if(found) {
						throw new IllegalInput("Non-unique ID detected. Please change your ID.");
					}
					else {
						int confirm = JOptionPane.showConfirmDialog(getRootPane(), 
								"Are you sure you want to create a Student with ID #" + e.getId() + "?");
						if(confirm == JOptionPane.YES_OPTION) {
							controller.addStudent(e);
							tabbedPane.setSelectedComponent(stuTablePanel);
							formPanel.clearFields();
							stuTablePanel.refresh();
							existingChanges = true;
						}
					}
				} 
				else if(e.getCmd().equals("Faculty")) {
					List<Student> students = controller.getStudents();
					List<Faculty> faculty = controller.getFaculty();
					boolean found = false;
					
					for(int i = 0; i < students.size(); i++) {
						String id = students.get(i).getId();
						if(id.equals(e.getId())) {
							found = true;
						}
					}
					for(int i = 0; i < faculty.size(); i++) {
						String id = faculty.get(i).getId();
						if(id.equals(e.getId())) {
							found = true;
						}
					}
					
					if(found) {
						throw new IllegalInput("Non-unique ID detected. Please change your ID.");
					}
					else {
						int confirm = JOptionPane.showConfirmDialog(getRootPane(), 
								"Are you sure you want to create a Faculty member with ID #" + e.getId() + "?");
						if(confirm == JOptionPane.YES_OPTION) {
							controller.addFaculty(e);
							tabbedPane.setSelectedComponent(facTablePanel);
							formPanel.clearFields();
							facTablePanel.refresh();
							existingChanges = true;
						}
					}
				}
				else if(e.getCmd().equals("Update Stu")) {
					List<Student> students = controller.getStudents();
					for(int i = 0; i < students.size(); i++) {
						String id = students.get(i).getId();
						if(id.equals(e.getId())) {
							controller.updateStudent(e, students.get(i));
							tabbedPane.setSelectedComponent(stuTablePanel);
							formPanel.clearFields();
							formPanel.setStudentVisible();
							stuTablePanel.refresh();
							existingChanges = true;
						}
					}
				}
				else {
					List<Faculty> faculty = controller.getFaculty();
					for(int i = 0; i < faculty.size(); i++) {
						String id = faculty.get(i).getId();
						if(id.equals(e.getId())) {
							controller.updateFaculty(e, faculty.get(i));
							tabbedPane.setSelectedComponent(facTablePanel);
							formPanel.clearFields();
							formPanel.setFacultyVisible();
							facTablePanel.refresh();
							existingChanges = true;
						}
					}
				}
			}
		});
		
		searchPanel.setSearchListener(new SearchListener() {
			public void searchEventOccurred(SearchEvent e) {
				searchPeople(e.getSearchId());
			}
		});
		
		// Displays a student in text area when selected in table
		stuTablePanel.setDisplaySelectedRowListener(new DisplaySelectedRowListener() {
			public void idOfSelectedRow(String id) {
				searchPeople(id);
			}
		});
		
		// Displays a faculty in text area when selected in table
		facTablePanel.setDisplaySelectedRowListener(new DisplaySelectedRowListener() {
			public void idOfSelectedRow(String id) {
				searchPeople(id);
			}
		});
		
		/* readies the FormPanel for an update from the user by displaying
		the appropriate fields for the object-to-be-updated */
		searchPanel.setUpdateListener(new UpdateListener());
		stuTablePanel.setUpdateSetupListener(new UpdateListener());
		facTablePanel.setUpdateSetupListener(new UpdateListener());
		
		//searches ArrayList<Person> for id then deletes object associated with id
		stuTablePanel.setTableDeleteListener(new DeleteListener());
		facTablePanel.setTableDeleteListener(new DeleteListener());
		
		tabbedPane.addTab("Students", stuTablePanel);
		tabbedPane.addTab("Faculty", facTablePanel);
		add(textPanel, BorderLayout.EAST);
		textPanel.setVisible(false);
		add(tabbedPane, BorderLayout.CENTER);
		add(formPanel, BorderLayout.WEST);
		add(toolbar, BorderLayout.NORTH);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setSize(1000, 500);
		setVisible(true);
		
		// listener that prevents user from unwittingly losing changes made in GUI
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if(!existingChanges) {
					try {
						controller.disconnect();
						System.exit(0);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else {
					int option = JOptionPane.showConfirmDialog(MainFrame.this, 
							"Would you like to save changes you have made to the database?", 
							"Exit Confirmation", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.WARNING_MESSAGE);
					if(option == JOptionPane.CANCEL_OPTION){
						//do nothing
					}
					else if(option == JOptionPane.YES_OPTION) {
						try {
							controller.saveFaculty();
							controller.saveStudents();
							controller.disconnect();
							System.exit(0);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
					else if(option == JOptionPane.NO_OPTION) {
						try {
							controller.disconnect();
							System.exit(0);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		
	}
	/**
	 * Creates a menu bar with the following options:
	 * 		Save current session data to database (Ctrl + S)
	 * 		Load data from database to current session (Ctrl + I)
	 * 		Database Preferences (Ctrl + P)
	 * 		Exit (Ctrl + X);
	 */
	private JMenuBar createMenuBar() {
		// File menu
		JMenu fileMenu = new JMenu("File");
		JMenuItem saveDataItem = new JMenuItem("Save Data...");
		JMenuItem loadDataItem = new JMenuItem("Load Data...");
		JMenuItem exitItem = new JMenuItem("Exit");
		fileMenu.add(saveDataItem);
		fileMenu.add(loadDataItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		
		//window menu
		JMenu windowMenu = new JMenu("Window");
		JMenuItem prefsItem = new JMenuItem("Preferences...");
		windowMenu.add(prefsItem);
		prefsItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				prefsDialog.setVisible(true);
			}
		});
		
		// Accelerators
		exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		loadDataItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		saveDataItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		prefsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		
		//load data listener
		loadDataItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					controller.loadStudents();
					controller.loadFaculty();
					stuTablePanel.refresh();
					facTablePanel.refresh();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		//save data listener
		saveDataItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					controller.saveStudents();
					controller.saveFaculty();
					existingChanges = false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		//exit listener that prevents user from unwittingly losing changes they have made this session
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(!existingChanges) {
					try {
						controller.disconnect();
						System.exit(0);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else {
					int option = JOptionPane.showConfirmDialog(MainFrame.this, 
							"Would you like to save changes you have made to the database?", 
							"Exit Confirmation", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.WARNING_MESSAGE);
					if(option == JOptionPane.CANCEL_OPTION){
						//do nothing
					}
					else if(option == JOptionPane.YES_OPTION) {
						try {
							controller.saveFaculty();
							controller.saveStudents();
							controller.disconnect();
							System.exit(0);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
					else if(option == JOptionPane.NO_OPTION) {
						try {
							controller.disconnect();
							System.exit(0);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(windowMenu);
		
		return menuBar;
	}
	/**
	 * Searches through student and faculty lists and displays information about them
	 * in a TextArea if found. If not found, an error is displayed.
	 */
	private void searchPeople(String id) {
		List<Student> students = controller.getStudents();
		List<Faculty> faculty = controller.getFaculty();
		String searchId = id;
		boolean found = false;
		
		for(int i = 0; i < students.size(); i++) {
			String instanceId = students.get(i).getId();
			if(instanceId.equals(searchId)) {
				textPanel.setVisible(false);
				textPanel.appendText(students.get(i).toString());
				textPanel.setVisible(true);
				found = true;
			}
		}
		for(int i = 0; i < faculty.size(); i++) {
			String instanceId = faculty.get(i).getId();
			if(instanceId.equals(searchId)) {
				textPanel.setVisible(false);
				textPanel.appendText(faculty.get(i).toString());
				textPanel.setVisible(true);
				found = true;
			}
		}
		if(found == false) {
			JOptionPane.showMessageDialog(getRootPane(), "Could not find anyone associated with that ID.");
		}
	}
	/**
	 * Searches through student and faculty lists using ID. Returns a Person object if found (to be
	 * cast to an appropriate class elsewhere) and a null Person object if not found.
	 */
	private Person searchAndRetrievePeople(String id) {
		List<Student> students = controller.getStudents();
		List<Faculty> faculty = controller.getFaculty();
		String searchId = id;
		
		for(int i = 0; i < students.size(); i++) {
			String instanceId = students.get(i).getId();
			if(instanceId.equals(searchId)) {
				return students.get(i);
			}
		}
		for(int i = 0; i < faculty.size(); i++) {
			String instanceId = faculty.get(i).getId();
			if(instanceId.equals(searchId)) {
				return faculty.get(i);
			}
		}
		Person person = null;
		return person;
	}
	/**
	 * Removes an object, by virtue of its associated ID, from both its associated LinkedList
	 * AND the SQL database
	 */
	private class DeleteListener implements TableDeleteListener {
		public void idOfDeletedRow(String id) {
			List<Faculty> faculty = controller.getFaculty();
			
			for(int i = 0; i < faculty.size(); i++) {
				if(faculty.get(i).getId() == id)
					try {
						controller.removeFaculty(i, id);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			stuTablePanel.refresh();
			facTablePanel.refresh();
		}
	}
	/**
	 * Prepares the FormPanel for an update by setting specific fields visible/invisible
	 * depending on what kind of object is being updated.
	 */
	private class UpdateListener implements UpdateSetupListener {
		public void updateSetupOccurred(UpdateSetup ue) {
			Person person = searchAndRetrievePeople(ue.getUpdateId());
			if(person != null) {
				if(person instanceof FullTimeStudent) {
					FullTimeStudent fts = (FullTimeStudent) person;
					formPanel.updateFTStu(fts);
				}
				if(person instanceof PartTimeStudent) {
					PartTimeStudent pts = (PartTimeStudent) person;
					formPanel.updatePTStu(pts);
				}
				if(person instanceof FullTimeFaculty) {
					FullTimeFaculty ftf = (FullTimeFaculty) person;
					formPanel.updateFTFac(ftf);
				}
				if(person instanceof PartTimeFaculty) {
					PartTimeFaculty ptf = (PartTimeFaculty) person;
					formPanel.updatePTFac(ptf);
				}
			}
		}
	}
}
