package view;

import java.util.EventObject;
/**
 *	The FormEvent class is used to capture data from the FormPanel class to then be
 *	unloaded in a separate class. Contains two different constructors based on whether
 *	the form being filled out is for a student or for faculty.
 */
public class FormEvent extends EventObject {
	private String cmd;
	private String id;
	private String name;
	private String address;
	private String phone;
	private String major;
	private String campus;
	private double gpa;
	private boolean isPTStu;
	private int creditsTaken;
	private String rank;
	private String office;
	private String officePhone;
	private boolean isPTFac;
	private double salary;
	private int creditsTaught;
	
	public FormEvent(Object source) {
		super(source);
	}
	
	// student constructor
	public FormEvent(Object source, String cmd, String id, String name, String address, String phone, String major,
			String campus, double gpa, boolean isPTStu, int creditsTaken) {
		super(source);
		this.cmd = cmd;
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.major = major;
		this.campus = campus;
		this.gpa = gpa;
		this.isPTStu = isPTStu;
		this.creditsTaken = creditsTaken;
	}

	// faculty constructor
	public FormEvent(Object source, String cmd, String id, String name, String address, String phone, String rank,
			String office, String officePhone, boolean isPTFac, double salary, int creditsTaught) {
		super(source);
		this.cmd = cmd;
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.rank = rank;
		this.office = office;
		this.officePhone = officePhone;
		this.isPTFac = isPTFac;
		this.salary = salary;
		this.creditsTaught = creditsTaught;
	}

	public String getId() {
		return id;
	}

	public String getCmd() {
		return cmd;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public String getMajor() {
		return major;
	}

	public String getCampus() {
		return campus;
	}

	public double getGpa() {
		return gpa;
	}

	public int getCreditsTaken() {
		return creditsTaken;
	}

	public String getRank() {
		return rank;
	}

	public String getOffice() {
		return office;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public double getSalary() {
		return salary;
	}

	public int getCreditsTaught() {
		return creditsTaught;
	}
	
	public boolean isPTStu() {
		return isPTStu;
	}

	public boolean isPTFac() {
		return isPTFac;
	}
	
}
