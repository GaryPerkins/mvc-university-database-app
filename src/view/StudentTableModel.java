package view;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.FullTimeStudent;
import model.PartTimeStudent;
import model.Student;
/**
 *	 Sets up the model for the JTable contained within StuTablePanel.
 */
public class StudentTableModel extends AbstractTableModel {
	private List<Student> students;
	private String[] colNames = {"ID", "Name", "Address", "Phone", 
			"Major", "Campus", "GPA", "Full Time", "Credits Taken", "Tuition"};
	
	public StudentTableModel() {
		
	}
	public List<Student> getStudents() {
		return students;
	}
	public String getColumnName(int column) {
		return colNames[column];
	}
	
	public void setData(List list) {
		students = list;
	}
	
	public int getColumnCount() {
		return colNames.length;
	}
	
	public int getRowCount() {
		return students.size();
	}
	/**
	 * Determines values to retrieve based on whether the object in the row is FullTime or PartTime.
	 */
	public Object getValueAt(int row, int col) {
		Student student = students.get(row);
		DecimalFormat df = new DecimalFormat("#.00");
		
		switch(col) {
		case 0:
			return student.getId();
		case 1:
			return student.getName();
		case 2:
			return student.getAddress();
		case 3:
			return student.getPhone();
		case 4:
			return student.getMajor();
		case 5:
			return student.getCampus();
		case 6:
			return student.getGpa();
		case 7: 
			if(student instanceof FullTimeStudent)
				return "\u2713";
			else
				return "X";
		case 8:
			if(student instanceof FullTimeStudent)
				return "N/A";
			else
				return ((PartTimeStudent) student).getCredits();
		case 9:
			if(student instanceof FullTimeStudent)
				return "$" + df.format(((FullTimeStudent) student).getTuition());
			else
				return "$" + df.format(((PartTimeStudent) student).getTuition());
		}
		return null;
	}
}
