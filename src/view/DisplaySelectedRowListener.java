package view;

import java.util.EventListener;
/**
 *	Interface for transmission of the ID number of a row to be displayed within a text area
 *	upon a user's clicking of that row. 
 */
public interface DisplaySelectedRowListener extends EventListener {
	public void idOfSelectedRow(String id);

}
