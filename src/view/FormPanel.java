package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import model.FullTimeFaculty;
import model.FullTimeStudent;
import model.PartTimeFaculty;
import model.PartTimeStudent;
/**
 *	JPanel that contains fields to be filled out in the creation or updating of Student or Faculty
 *	objects. Once a user hits the create or update button associated with each of object types
 *	(Student or Faculty), a FormEvent object is created for the transferral of this data.
 */
public class FormPanel extends JPanel {
	private JLabel idLabel;
	private JTextField idField;
	private JLabel nameLabel;
	private JTextField nameField;
	private JLabel addressLabel;
	private JTextField addressField;
	private JLabel phoneLabel;
	private JTextField phoneField;
	private JLabel majorLabel;
	private JTextField majorField;
	private JLabel campusLabel;
	private JComboBox campusComboBox;
	private JLabel gpaLabel;
	private JTextField gpaField;
	private JLabel creditsTakenLabel;
	private JTextField creditsTakenField;
	private JLabel rankLabel;
	private JComboBox rankComboBox;
	private JLabel officeAddressLabel;
	private JTextField officeAddressField;
	private JLabel officePhoneLabel;
	private JTextField officePhoneField;
	private JLabel salaryLabel;
	private JTextField salaryField;
	private JLabel creditsTaughtLabel;
	private JTextField creditsTaughtField;
	private JCheckBox partTimeStudentCheck;
	private JCheckBox partTimeFacultyCheck;
	private JLabel partTimeStudentLabel;
	private JLabel partTimeFacultyLabel;
	private JButton studentCreateBtn;
	private JButton facultyCreateBtn;
	private JButton studentUpdateBtn;
	private JButton facultyUpdateBtn;
	private JButton cancelUpdateStuBtn;
	private JButton cancelUpdateFacBtn;
	private FormListener formListener;
	
	public FormPanel() {
		Dimension dim = getPreferredSize();
		dim.width = 300;
		setPreferredSize(dim);
		
		createStudentBorder();
		
		idLabel = new JLabel("ID: ");
		idField = new JTextField(8);
		nameLabel = new JLabel("Name: ");
		nameField = new JTextField(10);
		addressLabel = new JLabel("Address: ");
		addressField = new JTextField(10);
		phoneLabel = new JLabel("Phone #: ");
		phoneField = new JTextField(10);
		majorLabel = new JLabel("Major: ");
		majorField = new JTextField(10);
		campusLabel = new JLabel("Campus: ");
		campusComboBox = new JComboBox();
		gpaLabel = new JLabel("GPA: ");
		gpaField = new JTextField(3);
		creditsTakenLabel = new JLabel("Credits taken: ");
		creditsTakenField = new JTextField(3);
		rankLabel = new JLabel("Rank: ");
		rankComboBox = new JComboBox();
		officeAddressLabel = new JLabel("Office: ");
		officeAddressField = new JTextField(10);
		officePhoneLabel = new JLabel("Office Phone: ");
		officePhoneField = new JTextField(10);
		salaryLabel = new JLabel("Salary: ");
		salaryField = new JTextField(10);
		creditsTaughtLabel = new JLabel("Credits taught: ");
		creditsTaughtField = new JTextField(3);
		partTimeStudentCheck = new JCheckBox();
		partTimeFacultyCheck = new JCheckBox();
		partTimeStudentLabel = new JLabel("Part Time: ");
		partTimeFacultyLabel = new JLabel("Part Time: ");
		studentCreateBtn = new JButton("Create");
		facultyCreateBtn = new JButton("Create");
		studentUpdateBtn = new JButton("Update");
		facultyUpdateBtn = new JButton("Update");
		cancelUpdateStuBtn = new JButton("Cancel");
		cancelUpdateFacBtn = new JButton("Cancel");
		
		// campus stuff
		DefaultComboBoxModel campusModel = new DefaultComboBoxModel();
		campusModel.addElement("Ammerman");
		campusModel.addElement("Grant");
		campusModel.addElement("East");
		campusComboBox.setModel(campusModel);
		campusComboBox.setSelectedIndex(0);
		campusComboBox.setEditable(false);
		
		// rank stuff
		DefaultComboBoxModel rankModel = new DefaultComboBoxModel();
		rankModel.addElement("Senior Professor");
		rankModel.addElement("Assistant Professor");
		rankModel.addElement("Newbie Professor");
		rankComboBox.setModel(rankModel);
		rankComboBox.setSelectedIndex(1);
		rankComboBox.setEditable(true);
		
		// part time student checkbox stuff
		creditsTakenLabel.setEnabled(false);
		creditsTakenField.setEnabled(false);
		partTimeStudentCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				boolean isChecked = partTimeStudentCheck.isSelected();
				creditsTakenLabel.setEnabled(isChecked);
				creditsTakenField.setEnabled(isChecked);
			}
		});
		
		// part time faculty checkbox stuff
		partTimeFacultyCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				boolean isChecked = partTimeFacultyCheck.isSelected();
				if(isChecked) {
					salaryLabel.setVisible(false);
					salaryField.setVisible(false);
					creditsTaughtLabel.setVisible(true);
					creditsTaughtField.setVisible(true);
				}
				else {
					creditsTaughtLabel.setVisible(false);
					creditsTaughtField.setVisible(false);
					salaryLabel.setVisible(true);
					salaryField.setVisible(true);
				}
			}
		});
		
		// student create button stuff
		studentCreateBtn.setActionCommand("Student");
		studentCreateBtn.addActionListener(new StudentFormEventListener());
		
		// faculty create button stuff
		facultyCreateBtn.setActionCommand("Faculty");
		facultyCreateBtn.addActionListener(new FacultyFormEventListener());
		
		studentUpdateBtn.setActionCommand("Update Stu");
		studentUpdateBtn.addActionListener(new StudentFormEventListener());
		
		facultyUpdateBtn.setActionCommand("Update Fac");
		facultyUpdateBtn.addActionListener(new FacultyFormEventListener());
		
		//cancel button stuff
		cancelUpdateStuBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				clearFields();
				idField.setEditable(true);
				partTimeFacultyCheck.setEnabled(true);
				setStudentVisible();
			}
		});
		
		//cancel button stuff
		cancelUpdateStuBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				clearFields();
				idField.setEditable(true);
				partTimeStudentCheck.setEnabled(true);
				setFacultyVisible();
			}
		});
		
		layoutComponent();
	}
	
	private class StudentFormEventListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			String cmd = ae.getActionCommand();
			String id = idField.getText();
			String name = nameField.getText();
			String address = addressField.getText();
			String phone = phoneField.getText();
			String major = majorField.getText();
			String campus = (String) campusComboBox.getSelectedItem();
			boolean isPTStu = partTimeStudentCheck.isSelected();
			
			/*checks to see if textfield is empty for primitive datatypes in order to determine
			if an exception needs to be thrown. if empty, will set values to -1. takes into account
			whether or not these empty fields are required to be filled out depending on if the
			intended object is to be FullTime or PartTime.*/
			if(!gpaField.getText().isEmpty() && !partTimeStudentCheck.isSelected()) {
				double gpa = Double.parseDouble(gpaField.getText());
				int creditsTaken = 12;
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, major, campus, 
						gpa, isPTStu, creditsTaken);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
			else if(!gpaField.getText().isEmpty() && partTimeStudentCheck.isSelected()
					&& !creditsTakenField.getText().isEmpty()) {
				double gpa = Double.parseDouble(gpaField.getText());
				int creditsTaken = Integer.parseInt(creditsTakenField.getText());
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, major, campus, 
						gpa, isPTStu, creditsTaken);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
			else if(!gpaField.getText().isEmpty() && partTimeStudentCheck.isSelected()
					&& creditsTakenField.getText().isEmpty()) {
				double gpa = Double.parseDouble(gpaField.getText());
				int creditsTaken = -1;
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, major, campus, 
						gpa, isPTStu, creditsTaken);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
			else {
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, major, campus, 
					-1, isPTStu, -1);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
		}
	}
	
	public class FacultyFormEventListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			String cmd = ae.getActionCommand();
			String id = idField.getText();
			String name = nameField.getText();
			String address = addressField.getText();
			String phone = phoneField.getText();
			String rank = (String) rankComboBox.getSelectedItem();
			String officeAddress = officeAddressField.getText();
			String officePhone = officePhoneField.getText();
			boolean isPTFac = partTimeFacultyCheck.isSelected();
			
			/*checks to see if textfield is empty for primitive datatypes in order to determine
			if an exception needs to be thrown. if empty, will set values to -1. takes into account
			whether or not these empty fields are required to be filled out depending on if the
			intended object is to be FullTime or PartTime.*/
			if(!salaryField.getText().isEmpty() && !partTimeFacultyCheck.isSelected()) {
				double salary = Double.parseDouble(salaryField.getText());
				int creditsTaught = 15;
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, rank, officeAddress, 
					officePhone, isPTFac, salary, creditsTaught);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
			else if(!creditsTaughtField.getText().isEmpty() && partTimeFacultyCheck.isSelected()) {
				double salary = 0;
				int creditsTaught = Integer.parseInt(creditsTaughtField.getText());
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, rank, officeAddress, 
					officePhone, isPTFac, salary, creditsTaught);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
			else {
				FormEvent ev = new FormEvent(this, cmd, id, name, address, phone, rank, officeAddress, 
						officePhone, isPTFac, -1, -1);
				if(formListener != null) {
					try {
						formListener.formEventOccurred(ev);
					} catch (IllegalInput e) {
						JOptionPane.showMessageDialog(getRootPane(), e.getMessage());
					}
				}
			}
		}
	}
	public void setFormListener(FormListener formListener) {
		this.formListener = formListener;
	}
	public void layoutComponent() {
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		// actual first row
		gc.gridy = 0;
		gc.gridx = 0;
		gc.weightx = 1;
		gc.weighty = 0.1;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(idLabel, gc);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(idField, gc);
		
		// First row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(nameLabel, gc);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(nameField, gc);
		
		// second row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(addressLabel, gc);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(addressField, gc);
		
		//third row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(phoneLabel, gc);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(phoneField, gc);
		
		//fourth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(majorLabel, gc);
		add(rankLabel, gc);
		rankLabel.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(majorField, gc);
		add(rankComboBox, gc);
		rankComboBox.setVisible(false);
		
		//fifth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(campusLabel, gc);
		add(officeAddressLabel, gc);
		officeAddressLabel.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(campusComboBox, gc);
		add(officeAddressField, gc);
		officeAddressField.setVisible(false);
		
		//sixth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(gpaLabel, gc);
		add(officePhoneLabel, gc);
		officePhoneLabel.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(gpaField, gc);
		add(officePhoneField, gc);
		officePhoneField.setVisible(false);
		
		// seventh row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(partTimeStudentLabel, gc);
		add(partTimeFacultyLabel, gc);
		partTimeFacultyLabel.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(partTimeStudentCheck, gc);
		add(partTimeFacultyCheck, gc);
		partTimeFacultyCheck.setVisible(false);
		
		// eighth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(creditsTakenLabel, gc);
		add(salaryLabel, gc);
		salaryLabel.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(creditsTakenField, gc);
		add(salaryField, gc);
		salaryField.setVisible(false);
		
		// ninth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(creditsTaughtLabel, gc);
		add(studentUpdateBtn, gc);
		creditsTaughtLabel.setVisible(false);
		studentUpdateBtn.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(studentCreateBtn, gc);
		add(cancelUpdateStuBtn, gc);
		cancelUpdateStuBtn.setVisible(false);
		add(creditsTaughtField, gc);
		creditsTaughtField.setVisible(false);
		
		// tenth row
		gc.gridy++;
		gc.gridx = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(facultyUpdateBtn, gc);
		facultyUpdateBtn.setVisible(false);
		
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(facultyCreateBtn, gc);
		add(cancelUpdateFacBtn, gc);
		cancelUpdateFacBtn.setVisible(false);
		facultyCreateBtn.setVisible(false);
	}
	public void setStudentVisible() {
		this.rankLabel.setVisible(false);
		this.rankComboBox.setVisible(false);
		this.officeAddressLabel.setVisible(false);
		this.officeAddressField.setVisible(false);
		this.officePhoneLabel.setVisible(false);
		this.officePhoneField.setVisible(false);
		this.partTimeFacultyLabel.setVisible(false);
		this.partTimeFacultyCheck.setVisible(false);
		this.salaryLabel.setVisible(false);
		this.salaryField.setVisible(false);
		this.creditsTaughtLabel.setVisible(false);
		this.creditsTaughtField.setVisible(false);
		this.facultyCreateBtn.setVisible(false);
		this.facultyUpdateBtn.setVisible(false);
		this.cancelUpdateFacBtn.setVisible(false);
		this.cancelUpdateStuBtn.setVisible(false);
		this.studentUpdateBtn.setVisible(false);
		
		this.idField.setEditable(true);
		this.partTimeStudentCheck.setEnabled(true);
		this.majorLabel.setVisible(true);
		this.majorField.setVisible(true);
		this.campusLabel.setVisible(true);
		this.campusComboBox.setVisible(true);
		this.gpaLabel.setVisible(true);
		this.gpaField.setVisible(true);
		this.partTimeStudentLabel.setVisible(true);
		this.partTimeStudentCheck.setVisible(true);
		this.creditsTakenLabel.setVisible(true);
		this.creditsTakenField.setVisible(true);
		this.studentCreateBtn.setVisible(true);
	}
	public void setFacultyVisible() {
		this.majorLabel.setVisible(false);
		this.majorField.setVisible(false);
		this.campusLabel.setVisible(false);
		this.campusComboBox.setVisible(false);
		this.gpaLabel.setVisible(false);
		this.gpaField.setVisible(false);
		this.partTimeStudentLabel.setVisible(false);
		this.partTimeStudentCheck.setVisible(false);
		this.creditsTakenLabel.setVisible(false);
		this.creditsTakenField.setVisible(false);
		this.studentCreateBtn.setVisible(false);
		this.facultyUpdateBtn.setVisible(false);
		this.cancelUpdateFacBtn.setVisible(false);
		this.cancelUpdateStuBtn.setVisible(false);
		this.studentUpdateBtn.setVisible(false);
		
		this.idField.setEditable(true);
		this.partTimeFacultyCheck.setEnabled(true);
		this.rankLabel.setVisible(true);
		this.rankComboBox.setVisible(true);
		this.officeAddressLabel.setVisible(true);
		this.officeAddressField.setVisible(true);
		this.officePhoneLabel.setVisible(true);
		this.officePhoneField.setVisible(true);
		this.partTimeFacultyLabel.setVisible(true);
		this.partTimeFacultyCheck.setVisible(true);
		this.salaryLabel.setVisible(true);
		this.salaryField.setVisible(true);
		this.creditsTaughtLabel.setVisible(false);
		this.creditsTaughtField.setVisible(false);
		this.facultyCreateBtn.setVisible(true);
	}
	
	public void updateFTStu(FullTimeStudent fts) {
		createStudentBorder();
		clearFields();
		this.rankLabel.setVisible(false);
		this.rankComboBox.setVisible(false);
		this.officeAddressLabel.setVisible(false);
		this.officeAddressField.setVisible(false);
		this.officePhoneLabel.setVisible(false);
		this.officePhoneField.setVisible(false);
		this.partTimeFacultyLabel.setVisible(false);
		this.partTimeFacultyCheck.setVisible(false);
		this.salaryLabel.setVisible(false);
		this.salaryField.setVisible(false);
		this.creditsTaughtLabel.setVisible(false);
		this.creditsTaughtField.setVisible(false);
		this.studentCreateBtn.setVisible(false);
		this.facultyCreateBtn.setVisible(false);
		this.facultyUpdateBtn.setVisible(false);
		this.cancelUpdateFacBtn.setVisible(false);
		
		this.idField.setEditable(false);
		this.majorLabel.setVisible(true);
		this.majorField.setVisible(true);
		this.campusLabel.setVisible(true);
		this.campusComboBox.setVisible(true);
		this.gpaLabel.setVisible(true);
		this.gpaField.setVisible(true);
		this.partTimeStudentLabel.setVisible(true);
		this.partTimeStudentCheck.setVisible(true);
		this.partTimeStudentCheck.setEnabled(false);
		this.partTimeStudentCheck.setSelected(false);
		this.creditsTakenLabel.setVisible(true);
		this.creditsTakenField.setVisible(true);
		this.studentUpdateBtn.setVisible(true);
		this.cancelUpdateStuBtn.setVisible(true);
		
		idField.setText(fts.getId());
		nameField.setText(fts.getName());
		addressField.setText(fts.getAddress());
		phoneField.setText(fts.getPhone());
		majorField.setText(fts.getMajor());
		int campusIndex;
		switch(fts.getCampus()) {
			case Ammerman:
				campusIndex = 0;
				break;
			case Grant:
				campusIndex = 1;
				break;
			case East:
				campusIndex = 2;
				break;
			default:
				campusIndex = 1;
		}
		campusComboBox.setSelectedIndex(campusIndex);
		gpaField.setText(String.valueOf(fts.getGpa()));
	}
	
	public void updatePTStu(PartTimeStudent pts) {
		createStudentBorder();
		clearFields();
		this.rankLabel.setVisible(false);
		this.rankComboBox.setVisible(false);
		this.officeAddressLabel.setVisible(false);
		this.officeAddressField.setVisible(false);
		this.officePhoneLabel.setVisible(false);
		this.officePhoneField.setVisible(false);
		this.partTimeFacultyLabel.setVisible(false);
		this.partTimeFacultyCheck.setVisible(false);
		this.salaryLabel.setVisible(false);
		this.salaryField.setVisible(false);
		this.creditsTaughtLabel.setVisible(false);
		this.creditsTaughtField.setVisible(false);
		this.studentCreateBtn.setVisible(false);
		this.facultyCreateBtn.setVisible(false);
		this.facultyUpdateBtn.setVisible(false);
		this.cancelUpdateFacBtn.setVisible(false);
		
		this.idField.setEditable(false);
		this.majorLabel.setVisible(true);
		this.majorField.setVisible(true);
		this.campusLabel.setVisible(true);
		this.campusComboBox.setVisible(true);
		this.gpaLabel.setVisible(true);
		this.gpaField.setVisible(true);
		this.partTimeStudentLabel.setVisible(true);
		this.partTimeStudentCheck.setVisible(true);
		this.partTimeStudentCheck.setEnabled(false);
		this.partTimeStudentCheck.setSelected(true);
		this.creditsTakenLabel.setVisible(true);
		this.creditsTakenField.setVisible(true);
		this.creditsTakenLabel.setEnabled(true);
		this.creditsTakenField.setEnabled(true);
		this.studentUpdateBtn.setVisible(true);
		this.cancelUpdateStuBtn.setVisible(true);
		
		idField.setText(pts.getId());
		nameField.setText(pts.getName());
		addressField.setText(pts.getAddress());
		phoneField.setText(pts.getPhone());
		majorField.setText(pts.getMajor());
		int campusIndex;
		switch(pts.getCampus()) {
			case Ammerman:
				campusIndex = 0;
				break;
			case Grant:
				campusIndex = 1;
				break;
			case East:
				campusIndex = 2;
				break;
			default:
				campusIndex = 1;
		}
		campusComboBox.setSelectedIndex(campusIndex);
		gpaField.setText(String.valueOf(pts.getGpa()));
		creditsTakenField.setText(String.valueOf(pts.getCredits()));
	}
	
	public void updateFTFac(FullTimeFaculty ftf) {
		createFacultyBorder();
		clearFields();
		this.majorLabel.setVisible(false);
		this.majorField.setVisible(false);
		this.campusLabel.setVisible(false);
		this.campusComboBox.setVisible(false);
		this.gpaLabel.setVisible(false);
		this.gpaField.setVisible(false);
		this.partTimeStudentLabel.setVisible(false);
		this.partTimeStudentCheck.setVisible(false);
		this.creditsTakenLabel.setVisible(false);
		this.creditsTakenField.setVisible(false);
		this.facultyCreateBtn.setVisible(false);
		this.studentCreateBtn.setVisible(false);
		this.studentUpdateBtn.setVisible(false);
		this.cancelUpdateStuBtn.setVisible(false);
		
		this.idField.setEditable(false);
		this.rankLabel.setVisible(true);
		this.rankComboBox.setVisible(true);
		this.officeAddressLabel.setVisible(true);
		this.officeAddressField.setVisible(true);
		this.officePhoneLabel.setVisible(true);
		this.officePhoneField.setVisible(true);
		this.partTimeFacultyLabel.setVisible(true);
		this.partTimeFacultyCheck.setVisible(true);
		this.partTimeFacultyCheck.setEnabled(false);
		this.partTimeFacultyCheck.setSelected(false);
		this.salaryLabel.setVisible(true);
		this.salaryField.setVisible(true);
		this.creditsTaughtLabel.setVisible(false);
		this.creditsTaughtField.setVisible(false);
		this.facultyUpdateBtn.setVisible(true);
		this.cancelUpdateFacBtn.setVisible(true);
		
		idField.setText(ftf.getId());
		nameField.setText(ftf.getName());
		addressField.setText(ftf.getAddress());
		phoneField.setText(ftf.getPhone());
		rankComboBox.setSelectedItem(ftf.getRank());
		officeAddressField.setText(ftf.getOfficeAddress());
		officePhoneField.setText(ftf.getOfficePhone());
		salaryField.setText(String.valueOf(ftf.getSalary()));
	}
	
	public void updatePTFac(PartTimeFaculty ptf) {
		createFacultyBorder();
		clearFields();
		this.majorLabel.setVisible(false);
		this.majorField.setVisible(false);
		this.campusLabel.setVisible(false);
		this.campusComboBox.setVisible(false);
		this.gpaLabel.setVisible(false);
		this.gpaField.setVisible(false);
		this.partTimeStudentLabel.setVisible(false);
		this.partTimeStudentCheck.setVisible(false);
		this.creditsTakenLabel.setVisible(false);
		this.creditsTakenField.setVisible(false);
		this.facultyCreateBtn.setVisible(false);
		this.studentCreateBtn.setVisible(false);
		this.studentUpdateBtn.setVisible(false);
		this.cancelUpdateStuBtn.setVisible(false);
		
		this.idField.setEditable(false);
		this.rankLabel.setVisible(true);
		this.rankComboBox.setVisible(true);
		this.officeAddressLabel.setVisible(true);
		this.officeAddressField.setVisible(true);
		this.officePhoneLabel.setVisible(true);
		this.officePhoneField.setVisible(true);
		this.partTimeFacultyLabel.setVisible(true);
		this.partTimeFacultyCheck.setVisible(true);
		this.partTimeFacultyCheck.setEnabled(false);
		this.partTimeFacultyCheck.setSelected(true);
		this.salaryLabel.setVisible(false);
		this.salaryField.setVisible(false);
		this.creditsTaughtLabel.setVisible(true);
		this.creditsTaughtField.setVisible(true);
		this.facultyUpdateBtn.setVisible(true);
		this.cancelUpdateFacBtn.setVisible(true);
		
		idField.setText(ptf.getId());
		nameField.setText(ptf.getName());
		addressField.setText(ptf.getAddress());
		phoneField.setText(ptf.getPhone());
		rankComboBox.setSelectedItem(ptf.getRank());
		officeAddressField.setText(ptf.getOfficeAddress());
		officePhoneField.setText(ptf.getOfficePhone());
		creditsTaughtField.setText(String.valueOf(ptf.getCreditsTaught()));
	}
	public void createStudentBorder() {
		Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		Border innerBorder = BorderFactory.createTitledBorder("Student Data");
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
	}
	
	public void createFacultyBorder() {
		Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		Border innerBorder = BorderFactory.createTitledBorder("Faculty Data");
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
	}
	
	/**
	 * Method for clearing fields at the end of an operation or a
	 * panel change
	 */
	public void clearFields() {
		idField.setText("");
		nameField.setText("");
		addressField.setText("");
		phoneField.setText("");
		majorField.setText("");
		campusComboBox.setSelectedIndex(0);
		gpaField.setText("");
		creditsTakenField.setText("");
		rankComboBox.setSelectedIndex(1);
		officeAddressField.setText("");
		officePhoneField.setText("");
		salaryField.setText("");
		creditsTaughtField.setText("");
	}
	public JTextField getNameField() {
		return nameField;
	}
	public void setNameField(JTextField nameField) {
		this.nameField = nameField;
	}
	public JTextField getAddressField() {
		return addressField;
	}
	public void setAddressField(JTextField addressField) {
		this.addressField = addressField;
	}
	public JTextField getPhoneField() {
		return phoneField;
	}
	public void setPhoneField(JTextField phoneField) {
		this.phoneField = phoneField;
	}
	public JTextField getMajorField() {
		return majorField;
	}
	public void setMajorField(JTextField majorField) {
		this.majorField = majorField;
	}
	public JComboBox getCampusComboBox() {
		return campusComboBox;
	}
	public void setCampusComboBox(JComboBox campusComboBox) {
		this.campusComboBox = campusComboBox;
	}
	public JTextField getGpaField() {
		return gpaField;
	}
	public void setGpaField(JTextField gpaField) {
		this.gpaField = gpaField;
	}
	public JTextField getCreditsTakenField() {
		return creditsTakenField;
	}
	public void setCreditsTakenField(JTextField creditsTakenField) {
		this.creditsTakenField = creditsTakenField;
	}
	public JComboBox getRankComboBox() {
		return rankComboBox;
	}
	public void setRankComboBox(JComboBox rankComboBox) {
		this.rankComboBox = rankComboBox;
	}
	public JTextField getOfficeAddressField() {
		return officeAddressField;
	}
	public void setOfficeAddressField(JTextField officeAddressField) {
		this.officeAddressField = officeAddressField;
	}
	public JTextField getOfficePhoneField() {
		return officePhoneField;
	}
	public void setOfficePhoneField(JTextField officePhoneField) {
		this.officePhoneField = officePhoneField;
	}
	public JTextField getSalaryField() {
		return salaryField;
	}
	public void setSalaryField(JTextField salaryField) {
		this.salaryField = salaryField;
	}
	public JTextField getCreditsTaughtField() {
		return creditsTaughtField;
	}
	public void setCreditsTaughtField(JTextField creditsTaughtField) {
		this.creditsTaughtField = creditsTaughtField;
	}
	public JCheckBox getPartTimeStudentCheck() {
		return partTimeStudentCheck;
	}
	public void setPartTimeStudentCheck(JCheckBox partTimeStudentCheck) {
		this.partTimeStudentCheck = partTimeStudentCheck;
	}
	public JCheckBox getPartTimeFacultyCheck() {
		return partTimeFacultyCheck;
	}
	public void setPartTimeFacultyCheck(JCheckBox partTimeFacultyCheck) {
		this.partTimeFacultyCheck = partTimeFacultyCheck;
	}
}
