package view;

import javax.swing.SwingUtilities;
/**
 * Thread-safe runner for the MainFrame class, wherein all user/program interactions take place.
 *
 */
public class App {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainFrame();
			}
		});
	}


}
