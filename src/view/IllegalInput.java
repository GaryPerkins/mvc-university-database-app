package view;
/**
 * Class for generating Exceptions relating to form data. Contains
 * a no-arg constructor for if the fields are blank and a custom 
 * constructor for specific error messages.
 */
public class IllegalInput extends Exception {
	
	public IllegalInput() {
		super("One or more fields are blank.");
	}
	public IllegalInput(String custom) {
		super(custom);
	}
}
