package view;

import java.text.DecimalFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Faculty;
import model.FullTimeFaculty;
import model.PartTimeFaculty;
import model.Person;
/**
 *	 Sets up the model for the JTable contained within FacTablePanel.
 */
public class FacultyTableModel extends AbstractTableModel {
	private List<Faculty> faculty;
	private String[] colNames = {"ID", "Name", "Address", "Phone", 
			"Rank", "Office Rm", "Office Phone", "Full Time", "Credits Taught", "Salary"};
	
	public FacultyTableModel() {
		
	}
	
	public String getColumnName(int column) {
		return colNames[column];
	}
	
	public void setData(List list) {
		this.faculty = list;
	}
	
	public int getColumnCount() {
		return colNames.length;
	}
	
	public int getRowCount() {
		return faculty.size();
	}
	/**
	 * Determines values to retrieve based on whether the object in the row is FullTime or PartTime.
	 */
	public Object getValueAt(int row, int col) {
		Person person = faculty.get(row);
		DecimalFormat df = new DecimalFormat("#.00");
		
		switch(col) {
		case 0:
			return person.getId();
		case 1:
			return person.getName();
		case 2:
			return person.getAddress();
		case 3:
			return person.getPhone();
		case 4:
			return ((Faculty) person).getRank();
		case 5:
			return ((Faculty) person).getOfficeAddress();
		case 6:
			return ((Faculty) person).getOfficePhone();
		case 7: 
			if(person instanceof FullTimeFaculty)
				return "\u2713";
			else
				return "X";
		case 8:
			if(person instanceof FullTimeFaculty)
				return "N/A";
			else
				return ((PartTimeFaculty) person).getCreditsTaught();
		case 9:
			if(person instanceof FullTimeFaculty)
				return "$" + df.format(((FullTimeFaculty) person).getSalary());
			else
				return "$" + df.format(((PartTimeFaculty) person).getWage());
		}
		return null;
	}

}
