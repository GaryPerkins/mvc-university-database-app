package view;

import java.util.EventListener;
/**
 *	Interface that allows for interaction between the SearchPanel within the toolbar
 *	and any other class. Unloads the id to be searched for via the SearchEvent object.
 */
public interface SearchListener extends EventListener {
	public void searchEventOccurred(SearchEvent se);
}
