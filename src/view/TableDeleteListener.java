package view;

import java.util.EventListener;
/**
 *	Interface that carries the ID of a row deleted within a JTable that
 *	is used to delete the Object associated with that ID in the database.
 */
public interface TableDeleteListener extends EventListener {
	public void idOfDeletedRow(String id);
}
