package view;

import java.util.EventObject;
/**
 * EventObject that carries the ID number of an object that is to be updated.
 * This ID is used in order to retrieve existing information about the object
 * to be put into the FormPanel in preparation for an update.
 */
public class UpdateSetup extends EventObject {
	private String updateId;
	public UpdateSetup(Object source, String updateId) {
		super(source);
		this.updateId = updateId;
	}
	
	public String getUpdateId() {
		return updateId;
	}

}
