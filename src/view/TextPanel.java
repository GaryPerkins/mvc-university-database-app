package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 * JPanel containing a JTextArea for displaying information about student/faculty
 * objects in memory. This textarea is invoked either through the searchbar or by
 * clicking on a row in the either the Student or Faculty table panels.
 *
 */
public class TextPanel extends JPanel {
	
	private JTextArea textArea;
	
	public TextPanel() {
		textArea = new JTextArea();
		textArea.setEditable(false);
		setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(200, 150));
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public void appendText(String text) {
		textArea.setText(text);
	}
}
