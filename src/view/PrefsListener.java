package view;
/**
 * 	An interface that allows interaction between PrefsDialog and MainFrame
 * 	(or any other class a developer desires). Carries preference information
 * 	as two strings (user and password) and a primitive integer (port).
 */
public interface PrefsListener {
	public void preferencesSet(String user, String password, int port);
}
