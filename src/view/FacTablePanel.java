package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Faculty;
/**
 *	Panel responsible for displaying Faculty data via a JTable.
 *	Is located within a JTabbedPane along with StuTablePanel.
 *	User can right click on a row to update or delete the object
 *	or can left click on a row to display the object it's associated
 *	with's information.
 */
public class FacTablePanel extends JPanel {
	private JTable table;
	private FacultyTableModel facultyModel;
	private JPopupMenu popup;
	private TableDeleteListener tableDeleteListener;
	private UpdateSetupListener updateSetupListener;
	private DisplaySelectedRowListener displaySelectedRowListener;
	
	public FacTablePanel() {
		facultyModel = new FacultyTableModel();
		table = new JTable(facultyModel);
		popup = new JPopupMenu();
		JMenuItem removeItem = new JMenuItem("Delete Faculty");
		JMenuItem updateItem = new JMenuItem("Update Faculty");
		popup.add(updateItem);
		popup.add(removeItem);
		
		table.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				table.getSelectionModel().setSelectionInterval(row, row);
				if(e.getButton() == MouseEvent.BUTTON3) {
					popup.show(table, e.getX(), e.getY());
				}
				if(e.getButton() == MouseEvent.BUTTON1) {
					int rowSelected = table.getSelectedRow();
					String id = (String) table.getValueAt(rowSelected, 0);
					if(displaySelectedRowListener != null) {
						displaySelectedRowListener.idOfSelectedRow(id);
					}
				}
			}
		});
		
		updateItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int row = table.getSelectedRow();
				String id = (String) table.getValueAt(row, 0);
				UpdateSetup ue = new UpdateSetup(this, id);
				if(updateSetupListener != null) {
					updateSetupListener.updateSetupOccurred(ue);
					facultyModel.fireTableDataChanged();
				}
			}
		});
		
		removeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int row = table.getSelectedRow();
				String id = (String) table.getValueAt(row, 0);
				if(tableDeleteListener != null) {
					tableDeleteListener.idOfDeletedRow(id);
					facultyModel.fireTableDataChanged();
				}
			}
		});
		setLayout(new BorderLayout());
		add(new JScrollPane(table), BorderLayout.CENTER);
	}
	
	public void setUpdateSetupListener(UpdateSetupListener updateSetupListener) {
		this.updateSetupListener = updateSetupListener;
	}
	
	public void setTableDeleteListener(TableDeleteListener tableDeleteListener) {
		this.tableDeleteListener = tableDeleteListener;
	}
	
	public void setDisplaySelectedRowListener(DisplaySelectedRowListener displaySelectedRowListener) {
		this.displaySelectedRowListener = displaySelectedRowListener;
	}
	
	public void setFacultyData(List list) {
		facultyModel.setData(list);
	}
	
	public void refresh() {
		facultyModel.fireTableDataChanged();
	}
}
