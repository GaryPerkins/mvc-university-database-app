package controller;

import java.util.List;

import model.Campus;
import model.Faculty;
import model.FullTimeFaculty;
import model.FullTimeStudent;
import model.PartTimeFaculty;
import model.PartTimeStudent;
import model.SQLDatabase;
import model.Student;
import view.FormEvent;
import view.IllegalInput;
/**
 *	Class responsible for manipulation of database via the MainFrame window class.
 */
public class Controller {	
	private SQLDatabase db = new SQLDatabase();
	
	public List getStudents() {
		return db.getStudents();
	}
	public List getFaculty() {
		return db.getFaculty();
	}
	public void removeStudent(int index, String id) throws Exception {
		db.removeStudent(index, id);
	}
	public void removeFaculty(int index, String id) throws Exception {
		db.removeFaculty(index, id);
	}
	public void loadStudents() throws Exception {
		db.loadStudents();
	}
	public void loadFaculty() throws Exception {
		db.loadFaculty();
	}
	public void saveStudents() throws Exception {
		db.saveStudents();
	}
	public void saveFaculty() throws Exception {
		db.saveFaculty();
	}
	public void connect() throws Exception {
		db.connect();
	}
	public void disconnect() throws Exception {
		db.disconnect();
	}
	
	/**
	 * Adds a student to the database using information gathered from the FormEvent object.
	 * Determines whether a student is Fulltime or Parttime based on the isPTStu instance field
	 * and adds accordingly.
	 */
	public void addStudent(FormEvent fe) throws IllegalInput {
		String id = fe.getId();
		String name = fe.getName();
		String address = fe.getAddress();
		String phone = fe.getPhone();
		String major = fe.getMajor();
		String campus = fe.getCampus();
		Campus campusEnum = null;
		if(campus.equals("Ammerman")) {
			campusEnum = Campus.Ammerman;
		} else if(campus.equals("Grant")) {
			campusEnum = Campus.Grant;
		} else {
			campusEnum = Campus.East;
		}
		double gpa = fe.getGpa();
		boolean isPTStu = fe.isPTStu();
		if(!isPTStu) {
			FullTimeStudent ftStu = new FullTimeStudent(id, name, address, phone, major, 
					campusEnum, gpa);
			db.addStudent(ftStu);
		} else {
			int creditsTaken = fe.getCreditsTaken();
			PartTimeStudent ptStu = new PartTimeStudent(id, name, address, phone, major,
					campusEnum, gpa, creditsTaken);
			db.addStudent(ptStu);
		}
	}
	/**
	 * Adds a faculty to the database using information gathered from the FormEvent object.
	 * Determines whether a faculty is Fulltime or Parttime based on the isPTFac instance field
	 * and adds accordingly.
	 */
	public void addFaculty(FormEvent fe) throws IllegalInput {
		String id = fe.getId();
		String name = fe.getName();
		String address = fe.getAddress();
		String phone = fe.getPhone();
		String rank = fe.getRank();
		String officeAddress = fe.getOffice();
		String officePhone = fe.getOfficePhone();
		boolean isPTFac = fe.isPTFac();
		if(!isPTFac) {
			double salary = fe.getSalary();
			FullTimeFaculty ftFac = new FullTimeFaculty(id, name, address, phone, rank,
					officeAddress, officePhone, salary);
			db.addFaculty(ftFac);
		} else {
			int creditsTaught = fe.getCreditsTaught();
			PartTimeFaculty ptFac = new PartTimeFaculty(id, name, address, phone, rank,
					officeAddress, officePhone, creditsTaught);
			db.addFaculty(ptFac);
		}
	}
	
	public void updateStudent(FormEvent fe, Student student) throws IllegalInput {
		String name = fe.getName();
		String address = fe.getAddress();
		String phone = fe.getPhone();
		String major = fe.getMajor();
		String campus = fe.getCampus();
		Campus campusEnum = null;
		if(campus.equals("Ammerman")) {
			campusEnum = Campus.Ammerman;
		} else if(campus.equals("Grant")) {
			campusEnum = Campus.Grant;
		} else {
			campusEnum = Campus.East;
		}
		double gpa = fe.getGpa();
		boolean isPTStu = fe.isPTStu();
		if(!isPTStu) {
			student.setName(name);
			student.setAddress(address);
			student.setPhone(phone);
			student.setMajor(major);
			student.setCampus(campusEnum);
			student.setGpa(gpa);
		}
		else {
			student.setName(name);
			student.setAddress(address);
			student.setPhone(phone);
			student.setMajor(major);
			student.setCampus(campusEnum);
			student.setGpa(gpa);
			((PartTimeStudent) student).setCredits(fe.getCreditsTaken());
		}
	}
	
	public void updateFaculty(FormEvent fe, Faculty faculty) throws IllegalInput {
		String name = fe.getName();
		String address = fe.getAddress();
		String phone = fe.getPhone();
		String rank = fe.getRank();
		String officeAddress = fe.getOffice();
		String officePhone = fe.getOfficePhone();
		boolean isPTFac = fe.isPTFac();
		if(!isPTFac) {
			faculty.setName(name);
			faculty.setAddress(address);
			faculty.setPhone(phone);
			faculty.setRank(rank);
			faculty.setOfficeAddress(officeAddress);
			faculty.setOfficePhone(officePhone);
			((FullTimeFaculty) faculty).setSalary(fe.getSalary());
		}
		else {
			faculty.setName(name);
			faculty.setAddress(address);
			faculty.setPhone(phone);
			faculty.setRank(rank);
			faculty.setOfficeAddress(officeAddress);
			faculty.setOfficePhone(officePhone);
			((PartTimeFaculty) faculty).setCreditsTaught(fe.getCreditsTaught());
		}
	}
}
