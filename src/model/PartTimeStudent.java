package model;

import java.io.Serializable;
import java.text.DecimalFormat;

import view.IllegalInput;

public class PartTimeStudent extends Student implements Serializable {
	private int credits;
	private double tuition;
	private final boolean isFullTime = false;

	public PartTimeStudent(String id, String name, String address, String phone,
			String major, Campus campus, double gpa, int credits) throws IllegalInput {
		super(id, name, address, phone, major, campus, gpa);
		if(credits > 11)
			throw new IllegalInput("You cannot take more than 11 credits and be considered a Part Time Student.");
		if(credits < 0)
			throw new IllegalInput();
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) throws IllegalInput {
		if(credits > 11)
			throw new IllegalInput("You cannot take more than 11 credits and be considered a Part Time Student.");
		this.credits = credits;
	}

	public double getTuition() {
		return credits * 180;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return "Name: " + getName() + "\nAddress: " + getAddress() + "\nPhone: " + getPhone() + 
				"\nMajor: " + getMajor() + "\nCampus: " + getCampus() + "\nGPA: " + getGpa() + "\nCredits Taken: " +
				getCredits() + "\nTuition: $" + df.format(getTuition());
	}
	
	public boolean isFullTime() {
		return isFullTime;
	}
}
