package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;

import view.IllegalInput;

public class SQLDatabase {
	private List<Student> students;
	private List<Faculty> faculty;
	
	private Connection con;
	
	public SQLDatabase() {
		students = new LinkedList<Student>();
		faculty = new LinkedList<Faculty>();
	}
	
	public void addStudent(Student stu) {
		students.add(stu);
	}
	
	public void addFaculty(Faculty fac) {
		faculty.add(fac);
	}
	
	public List getStudents() {
		return students;
	}
	
	public List getFaculty() {
		return faculty;
	}
	/**
	 * Attempts to connect to MySQL database. If credentials are invalid, displays an error
	 * window asking to reattempt login.
	 */
	public void connect() throws ClassNotFoundException, SQLException {
		if(con != null) return;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException("Driver not found");
		}
		
		// gets preferences if they exist, otherwise gets defaults
		String user = Preferences.userRoot().node("db").get("user", "");
		String pass = Preferences.userRoot().node("db").get("password", "");
		int port = Preferences.userRoot().node("db").getInt("port", 3306);
		
		String url = "jdbc:mysql://localhost:" + String.valueOf(port) + "/StudentFacultyDB";
		
		try {
			con = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Access Denied. Try again using valid credentials.\n"
					+ "To set your Preferences go to Window > Preferences or hit Ctrl+P.");
		}
	}
	
	public void disconnect() {
		if(con != null) {
			try {
				con.close();
			} catch(SQLException e) {
				System.out.println("Cannot close connection");
			}
		}
	}
	
	public void removeStudent(int index, String id) throws Exception {
		connect();
		students.remove(index);
		int removedId = Integer.parseInt(id);
		String deleteSql = "delete from students where id=" + removedId;
		PreparedStatement deleteStmt = con.prepareStatement(deleteSql);
		System.out.println("Deleting student with ID " + removedId);
		deleteStmt.executeUpdate();
	}
	
	public void removeFaculty(int index, String id) throws Exception {
		connect();
		faculty.remove(index);
		int removedId = Integer.parseInt(id);
		String deleteSql = "delete from faculty where id=" + removedId;
		PreparedStatement deleteStmt = con.prepareStatement(deleteSql);
		deleteStmt.executeUpdate();
	}
	
	/**
	 * Connects to MySQL and iterates through the student table to determine if any student
	 * within the current LinkedList differs from the info in the database. If an ID is matched,
	 * this method will update any instance field that has changed since the database's last 
	 * modification. If no ID is matched, the student object is added to the database.
	 */
	public void saveStudents() throws Exception {
		connect();
		String checkSql = "select count(*) as count from students where id=?";
		PreparedStatement checkStmt = con.prepareStatement(checkSql);
		
		String insertSql = "insert into students (id, name, address, phone, "
				+ "major, campus, gpa, part_time, credits_taken) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement insertStatement = con.prepareStatement(insertSql);
		
		String updateSql = "update students set name=?, address=?, phone=?, major=?, campus=?, "
				+ "gpa=?, part_time=?, credits_taken=? where id=?";
		PreparedStatement updateStatement = con.prepareStatement(updateSql);
		
		for(Student student: students) {
			String id = student.getId();
			String name = student.getName();
			String address = student.getAddress();
			String phone = student.getPhone();
			String major = student.getMajor();
			Campus campus = student.getCampus();
			double gpa = student.getGpa();
			boolean isPt;
			int creditsTaken = -1;
			if(student instanceof FullTimeStudent) {
				isPt = false;
				creditsTaken = -1;
			}
			else {
				isPt = true;
				creditsTaken = ((PartTimeStudent) student).getCredits();
			}
			
			checkStmt.setInt(1, Integer.parseInt(id));
			ResultSet checkResult = checkStmt.executeQuery();
			checkResult.next();
			
			int count = checkResult.getInt(1);
			
			if(count == 0) {
				//System.out.println("Inserting student with ID " + id);
				
				int col = 1;
				insertStatement.setInt(col++, Integer.parseInt(id));
				insertStatement.setString(col++, name);
				insertStatement.setString(col++, address);
				insertStatement.setString(col++, phone);
				insertStatement.setString(col++, major);
				insertStatement.setString(col++, campus.name());
				insertStatement.setDouble(col++, gpa);
				insertStatement.setBoolean(col++, isPt);
				insertStatement.setInt(col++, creditsTaken);
				
				insertStatement.executeUpdate();
			} else {
				//System.out.println("Updating student with ID " + id);
				int col = 1;
				
				updateStatement.setString(col++, name);
				updateStatement.setString(col++, address);
				updateStatement.setString(col++, phone);
				updateStatement.setString(col++, major);
				updateStatement.setString(col++, campus.name());
				updateStatement.setDouble(col++, gpa);
				updateStatement.setBoolean(col++, isPt);
				updateStatement.setInt(col++, creditsTaken);
				updateStatement.setString(col++, id);
				
				updateStatement.executeUpdate();
			}
		}
		updateStatement.close();
		insertStatement.close();
		checkStmt.close();
	}
	
	/**
	 * Connects to MySQL and iterates through the faculty table to determine if any faculty
	 * within the current LinkedList differs from the info in the database. If an ID is matched,
	 * this method will update any instance field that has changed since the database's last 
	 * modification. If no ID is matched, the faculty object is added to the database.
	 */
	public void saveFaculty() throws Exception {
		connect();
		String checkSql = "select count(*) as count from faculty where id=?";
		PreparedStatement checkStmt = con.prepareStatement(checkSql);
		
		String insertSql = "insert into faculty (id, name, address, phone, "
				+ "rank, office_address, office_phone, part_time, salary, credits_taught) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement insertStatement = con.prepareStatement(insertSql);
		
		String updateSql = "update faculty set name=?, address=?, phone=?, rank=?, office_address=?, "
				+ "office_phone=?, part_time=?, salary=?, credits_taught=? where id=?";
		PreparedStatement updateStatement = con.prepareStatement(updateSql);
		
		for(Faculty fac: faculty) {
			String id = fac.getId();
			String name = fac.getName();
			String address = fac.getAddress();
			String phone = fac.getPhone();
			String rank = fac.getRank();
			String office = fac.getOfficeAddress();
			String officePhone = fac.getOfficePhone();
			boolean isPt;
			double salary;
			int creditsTaught;
			if(fac instanceof FullTimeFaculty) {
				isPt = false;
				salary = ((FullTimeFaculty) fac).getSalary();
				creditsTaught = -1;
			}
			else {
				isPt = true;
				salary = -1;
				creditsTaught = ((PartTimeFaculty) fac).getCreditsTaught();
			}
			
			checkStmt.setInt(1, Integer.parseInt(id));
			ResultSet checkResult = checkStmt.executeQuery();
			checkResult.next();
			
			int count = checkResult.getInt(1);
			
			if(count == 0) {
				//System.out.println("Inserting faculty with ID " + id);
				
				int col = 1;
				insertStatement.setInt(col++, Integer.parseInt(id));
				insertStatement.setString(col++, name);
				insertStatement.setString(col++, address);
				insertStatement.setString(col++, phone);
				insertStatement.setString(col++, rank);
				insertStatement.setString(col++, office);
				insertStatement.setString(col++, officePhone);
				insertStatement.setBoolean(col++, isPt);
				insertStatement.setDouble(col++, salary);
				insertStatement.setInt(col++, creditsTaught);
				
				insertStatement.executeUpdate();
			} else {
				//System.out.println("Updating faculty with ID " + id);
				int col = 1;
				
				updateStatement.setString(col++, name);
				updateStatement.setString(col++, address);
				updateStatement.setString(col++, phone);
				updateStatement.setString(col++, rank);
				updateStatement.setString(col++, office);
				updateStatement.setString(col++, officePhone);
				updateStatement.setBoolean(col++, isPt);
				updateStatement.setDouble(col++, salary);
				updateStatement.setInt(col++, creditsTaught);
				updateStatement.setString(col++, id);
				
				updateStatement.executeUpdate();
			}
		}
		updateStatement.close();
		insertStatement.close();
		checkStmt.close();
	}
	/**
	 * 	Loads student objects from the database into memory via the students LinkedList.
	 * 	If there are any existing objects in memory, those objects will be lost upon 
	 * 	loading from the database.
	 */
	public void loadStudents() throws Exception {
			connect();
			students.clear();
			String sql = "select id, name, address, phone, major, campus, gpa, part_time, credits_taken from students order by name";
			Statement selectStatement = con.createStatement();
			ResultSet results = selectStatement.executeQuery(sql);
			while(results.next()) {
				int id = results.getInt("id");
				String name = results.getString("name");
				String address = results.getString("address");
				String phone = results.getString("phone");
				String major = results.getString("major");
				String campus = results.getString("campus");
				double gpa = results.getDouble("gpa");
				boolean isPt = results.getBoolean("part_time");
				int creditsTaken = results.getInt("credits_taken");
				
				if(isPt) {
					try {
						PartTimeStudent stu = new PartTimeStudent(String.valueOf(id), name, address, phone, major, Campus.valueOf(campus), gpa, creditsTaken);
						students.add(stu);
					} catch (IllegalInput e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else {
					try {
						FullTimeStudent stu = new FullTimeStudent(String.valueOf(id), name, address, phone, major, Campus.valueOf(campus), gpa);
						students.add(stu);
					} catch (IllegalInput e) {
						e.printStackTrace();
					}
				}
			}
		
			results.close();
			selectStatement.close();
	}
	/**
	 * 	Loads faculty objects from the database into memory via the students LinkedList.
	 * 	If there are any existing objects in memory, those objects will be lost upon 
	 * 	loading from the database.
	 */
	public void loadFaculty() throws Exception {
		connect();
		faculty.clear();
		String sql = "select id, name, address, phone, rank, office_address, office_phone, part_time, salary, credits_taught from faculty order by name";
		Statement selectStatement = con.createStatement();
		ResultSet results = selectStatement.executeQuery(sql);
		while(results.next()) {
			int id = results.getInt("id");
			String name = results.getString("name");
			String address = results.getString("address");
			String phone = results.getString("phone");
			String rank = results.getString("rank");
			String officeAddress = results.getString("office_address");
			String officePhone = results.getString("office_phone");
			boolean isPt = results.getBoolean("part_time");
			double salary = results.getDouble("salary");
			int creditsTaught = results.getInt("credits_taught");
			
			if(isPt) {
				try {
					PartTimeFaculty fac = new PartTimeFaculty(String.valueOf(id), name, address, phone, rank, officeAddress, officePhone, creditsTaught);
					faculty.add(fac);
				} catch (IllegalInput e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				try {
					FullTimeFaculty fac = new FullTimeFaculty(String.valueOf(id), name, address, phone, rank, officeAddress, officePhone, salary);
					faculty.add(fac);
				} catch (IllegalInput e) {
					e.printStackTrace();
				}
			}
		}
		
		results.close();
		selectStatement.close();
	}
}
