package model;

import java.io.Serializable;
import java.text.DecimalFormat;

import view.IllegalInput;

public class PartTimeFaculty extends Faculty implements Serializable {
	private int creditsTaught;
	private double wage;
	private final boolean isFullTime = false;
	
	public PartTimeFaculty(String id, String name, String address, String phone, String rank, 
			String officeAddress, String officePhone, int creditsTaught) throws IllegalInput {
		super(id, name, address, phone, rank, officeAddress, officePhone);
		if(creditsTaught == -1)
			throw new IllegalInput();
		this.creditsTaught = creditsTaught;
	}
	public int getCreditsTaught() {
		return creditsTaught;
	}
	public void setCreditsTaught(int creditsTaught) throws IllegalInput {
		if(creditsTaught == -1)
			throw new IllegalInput();
		this.creditsTaught = creditsTaught;
	}
	public double getWage() {
		return creditsTaught * 1000;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return "Name: " + getName() + "\nAddress: " + getAddress() + "\nPhone: " + getPhone() + 
				"\nRank: " + getRank() + "\nOffice: " + getOfficeAddress() + "\nOffice Phone: " +
				getOfficePhone() + "\nCredits Taught: " + getCreditsTaught() + "\nWage: $" + df.format(getWage());
	}
	
	public boolean isFullTime() {
		return isFullTime;
	}
}
