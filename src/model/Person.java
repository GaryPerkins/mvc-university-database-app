package model;

import java.io.Serializable;

import view.IllegalInput;

public class Person implements Serializable {
	
	private String name;
	private String address;
	private String phone;
	private String id;
	
	public Person() {
		
	}
	public Person(String id, String name, String address, String phone) throws IllegalInput {
		if(id.equals(""))
			throw new IllegalInput();
		if(id.length() != 8)
			throw new IllegalInput("IDs must be 8 digits long.");
		if(name.equals(""))
			throw new IllegalInput();
		if(address.equals(""))
			throw new IllegalInput();
		if(phone.equals(""))
			throw new IllegalInput();
		if (phone.replaceAll("-", "").length() < 10 || phone.replaceAll("-", "").length() > 10)
			throw new IllegalInput("Phone number must be 10 digits long.");
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) throws IllegalInput {
		if(name.equals(""))
			throw new IllegalInput();
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) throws IllegalInput {
		if(address.equals(""))
			throw new IllegalInput();
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) throws IllegalInput {
		if(phone.equals(""))
			throw new IllegalInput();
		if (phone.replaceAll("-", "").length() < 10 || phone.replaceAll("-", "").length() > 10)
			throw new IllegalInput("Phone number must be 10 digits long.");
		this.phone = phone;
	}
	public String getId() {
		return id;
	}
}
