package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * TODO
 */
public class Database {
	private ArrayList<Person> people;
	private ArrayList<Student> students;
	private ArrayList<Faculty> faculty;
	
	public Database() {
		people = new ArrayList<Person>();
		students = new ArrayList<Student>();
		faculty = new ArrayList<Faculty>();
	}
	
	public void addPerson(Person person) {
		people.add(person);
		if(person instanceof FullTimeStudent || person instanceof PartTimeStudent)
			students.add((Student) person);
		else
			faculty.add((Faculty) person);
	}
	
	public void removeStudent(int index) {
		students.remove(index);
	}
	
	public void removeFaculty(int index) {
		faculty.remove(index);
	}
	
	public void removePerson(int index) {
		people.remove(index);
	}
	
	public void importStudentList() {
		for(int i = 0; i < people.size(); i++) {
			if(people.get(i) instanceof FullTimeStudent || people.get(i) instanceof PartTimeStudent)
				students.add((Student) people.get(i));
		}
	}
	
	public void importFacultyList() {
		for(int i = 0; i < people.size(); i++) {
			if(people.get(i) instanceof FullTimeFaculty || people.get(i) instanceof PartTimeFaculty)
				faculty.add((Faculty) people.get(i));
		}
	}
	
	public ArrayList<Student> getStudents() {
		return students;
	}
	
	public ArrayList<Faculty> getFaculty() {
		return faculty;
	}
	
	public ArrayList<Person> getPeople() {
		return people;
	}
	
	public void saveToFile(File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(people);
		oos.close();
	}
	
	public void readFromFile(File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		try {
			people = (ArrayList<Person>) ois.readObject();
			importStudentList();
			importFacultyList();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		ois.close();
	}
}
