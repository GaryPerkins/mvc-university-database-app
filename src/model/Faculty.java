package model;

import java.io.Serializable;

import view.IllegalInput;

public class Faculty extends Person implements Serializable {
	private String rank;
	private String officeAddress;
	private String officePhone;
	
	public Faculty(String id, String name, String address, String phone, String rank, 
			String officeAddress, String officePhone) throws IllegalInput {
		super(id, name, address, phone);
		if(rank.equals(""))
			throw new IllegalInput();
		if(officeAddress.equals(""))
			throw new IllegalInput();
		if(officePhone.equals(""))
			throw new IllegalInput();
		if (officePhone.replaceAll("-", "").length() < 10 || officePhone.replaceAll("-", "").length() > 10)
			throw new IllegalInput("Phone number must be 10 digits long.");
		this.rank = rank;
		this.officeAddress = officeAddress;
		this.officePhone = officePhone;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) throws IllegalInput {
		if(rank.equals(""))
			throw new IllegalInput();
		this.rank = rank;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) throws IllegalInput {
		if(officeAddress.equals(""))
			throw new IllegalInput();
		this.officeAddress = officeAddress;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) throws IllegalInput {
		if(officePhone.equals(""))
			throw new IllegalInput();
		if (officePhone.replaceAll("-", "").length() < 10 || officePhone.replaceAll("-", "").length() > 10)
			throw new IllegalInput("Phone number must be 10 digits long.");
		this.officePhone = officePhone;
	}
	
	
}
