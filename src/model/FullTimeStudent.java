package model;

import java.io.Serializable;
import java.text.DecimalFormat;

import view.IllegalInput;

public class FullTimeStudent extends Student implements Serializable {
	
	private final double tuition = 3200.00;
	private final boolean isFullTime = true;
	
	public FullTimeStudent(String id, String name, String address, String phone, String major,
			Campus campus, double gpa) throws IllegalInput {
		super(id, name, address, phone, major, campus, gpa);
	}

	public double getTuition() {
		return tuition;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return "Name: " + getName() + "\nAddress: " + getAddress() + "\nPhone: " + getPhone() + 
				"\nMajor: " + getMajor() + "\nCampus: " + getCampus() + "\nGPA: " + getGpa() + 
				"\nTuition: $" + df.format(tuition);
	}
	
	public boolean isFullTime() {
		return isFullTime;
	}
}
