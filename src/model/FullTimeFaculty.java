package model;

import java.io.Serializable;
import java.text.DecimalFormat;

import view.IllegalInput;

public class FullTimeFaculty extends Faculty implements Serializable {
	private double salary;
	private final boolean isFullTime = true;
	
	public FullTimeFaculty(String id, String name, String address, String phone, 
			String rank, String officeAddress, String officePhone, double salary) throws IllegalInput {
		super(id, name, address, phone, rank, officeAddress, officePhone);
		if(salary == -1)
			throw new IllegalInput();
		this.salary = salary;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) throws IllegalInput {
		if(salary == -1)
			throw new IllegalInput();
		this.salary = salary;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return "Name: " + getName() + "\nAddress: " + getAddress() + "\nPhone: " + getPhone() + 
				"\nRank: " + getRank() + "\nOffice: " + getOfficeAddress() + "\nOffice Phone: " +
				getOfficePhone() + "\nSalary: $" + df.format(salary);
	}
	
	public boolean isFullTime() {
		return isFullTime;
	}
}
