package model;

import java.io.Serializable;

import view.IllegalInput;

public class Student extends Person implements Serializable {
	private String major;
	private Campus campus;
	private double gpa;
	
	public Student(String id, String name, String address, String phone, String major, 
			Campus campus, double gpa) throws IllegalInput {
		super(id, name, address, phone);
		if(major.equals(""))
			throw new IllegalInput();
		if(gpa < 0 || gpa > 4.0)
			throw new IllegalInput("GPA must be between 0 and 4.0.");
		this.major = major;
		this.campus = campus;
		this.gpa = gpa;
	}
	
	public String getMajor() {
		return major;
	}

	public void setMajor(String major) throws IllegalInput {
		if(major.equals(""))
			throw new IllegalInput();
		this.major = major;
	}

	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) throws IllegalInput {
		if(gpa < 0 || gpa > 4.0)
			throw new IllegalInput("GPA must be between 0 and 4.0.");
		this.gpa = gpa;
	}

}
