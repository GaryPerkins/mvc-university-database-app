# MVC University Database Application #

Administration system designed to simplify storing information about faculty and students enrolled in/teaching at a college

* Stored information based on ID number and was able to store object-based information in MySQL database
* Able to retrieve stored information and elegantly display it to the user
* Special attention was paid to database security, e.g. data sanitization to protect against SQL injection 
* Knowledge of OOP principles such as polymorphism and inheritance are aptly demonstrated

## Requirements ##
* MySQL JDBC driver
* MySQL Server running on localhost

## Questions/Comments? ##
gary.perkins1164@gmail.com